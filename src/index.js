import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});

app.get('/task2a', (req, res) => {
  const sum = (+req.query.a || 0) + (+req.query.b || 0);
  res.send(sum.toString());
});

app.get('/task2b', (req, res) => {
  const fullname = req.query.fullname.trim();
  const splitted2 = fullname.split(' ');
  let result = 'Invalid fullname';

  const splitted = [];
  for (var i = 0; i < splitted2.length; i++) {
    if (splitted2[i] == '') continue;
    splitted.push(splitted2[i]);
  }

  if (splitted.length > 3 || splitted.length < 1) {
    res.send(result);
  } else {
    let surname = splitted[splitted.length - 1].trim();
    let name1 = splitted[0].trim();

    const re = /^[^\s\d_/]+$/;
    const checked_name1 = name1.match(re);

    if (!checked_name1) {
      result = 'Invalid fullname';
      res.send(result);
      return;
    }

    const checked_surname = surname.match(re);

    if (!checked_surname) {
      result = 'Invalid fullname';
      res.send(result);
      return;
    }

    surname = surname.charAt(0).toUpperCase() + surname.slice(1).toLowerCase();

    switch (splitted.length) {
      case 1:
        result = surname;
        if (result == '') {
          result = 'Invalid fullname';
        }
        break;
      case 2:
        name1 = name1.charAt(0).toUpperCase();
        result = `${surname} ${name1}.`;
        break;
      case 3:
        name1 = name1.charAt(0).toUpperCase();
        let name2 = splitted[1].trim();

        const checked_name2 = name2.match(re)

        if (!checked_name2) {
          result = 'Invalid fullname';
          break;
        }

        name2 = name2.charAt(0).toUpperCase();
        result = `${surname} ${name1}. ${name2}.`;
        break;
    }
  }

  res.send(result);
});

app.get('/task2c', (req, res) => {
  let username = req.query.username.trim();
  let result = 'Invalid fullname';

  const doubleSlashIndex = username.indexOf('//');

  if (doubleSlashIndex >= 0) {
    username = username.slice(doubleSlashIndex + 2);

    let firstSlashIndex = username.indexOf('/');

    if (firstSlashIndex >= 0) {
      username = username.slice(firstSlashIndex + 1);

      firstSlashIndex = username.indexOf('/');

      if (firstSlashIndex >= 0) {
        username = username.slice(0, firstSlashIndex);
      }
    }

    const questionIndex = username.indexOf('?');

    if (questionIndex >= 0) {
      username = username.slice(0, questionIndex);
    }
  } else {
    let firstSlashIndex = username.indexOf('/');

    if (firstSlashIndex >= 0) {
      username = username.slice(firstSlashIndex + 1);

      firstSlashIndex = username.indexOf('/');

      if (firstSlashIndex >= 0) {
        username = username.slice(0, firstSlashIndex);
      }
    } else {
      if (username.indexOf('@') == 0) {
        if (username.length == 1) {
          res.send(result);
        } else {
          username = username.slice(1);
        }
      }
    }
  }

  let startSymbol = '@';
  if (username.indexOf('@') == 0) {
    startSymbol = '';
  }

  result = `${startSymbol}${username}`;
  res.send(result);
});

app.get('/task3a/volumes', (req, res) => {
  const pc = { "board": { "vendor": "IBM", "model": "IBM-PC S-100", "cpu": { "model": "80286", "hz": 12000 }, "image": "http://www.s100computers.com/My%20System%20Pages/80286%20Board/Picture%20of%2080286%20V2%20BoardJPG.jpg", "video": "http://www.s100computers.com/My%20System%20Pages/80286%20Board/80286-Demo3.mp4" }, "ram": { "vendor": "CTS", "volume": 1048576, "pins": 30 }, "os": "MS-DOS 1.25", "floppy": 0, "hdd": [{ "vendor": "Samsung", "size": 33554432, "volume": "C:" }, { "vendor": "Maxtor", "size": 16777216, "volume": "D:" }, { "vendor": "Maxtor", "size": 8388608, "volume": "C:" }], "monitor": null, "length": 42, "height": 21, "width": 54 };

  let volumes = {};
  let volumes2 = {};

  for (let i = 0; i < pc.hdd.length; i++) {
    if (pc.hdd[i].volume in volumes) {
      volumes[pc.hdd[i].volume] = volumes[pc.hdd[i].volume] + pc.hdd[i].size;
    } else {
      volumes[pc.hdd[i].volume] = pc.hdd[i].size;
    }
  }
  
  for (let k in volumes) {
    volumes2[k] = `${volumes[k]}B`;
  }

  res.json(volumes2);
});

app.get('/task3a*', (req, res) => {
  const pc = { "board": { "vendor": "IBM", "model": "IBM-PC S-100", "cpu": { "model": "80286", "hz": 12000 }, "image": "http://www.s100computers.com/My%20System%20Pages/80286%20Board/Picture%20of%2080286%20V2%20BoardJPG.jpg", "video": "http://www.s100computers.com/My%20System%20Pages/80286%20Board/80286-Demo3.mp4" }, "ram": { "vendor": "CTS", "volume": 1048576, "pins": 30 }, "os": "MS-DOS 1.25", "floppy": 0, "hdd": [{ "vendor": "Samsung", "size": 33554432, "volume": "C:" }, { "vendor": "Maxtor", "size": 16777216, "volume": "D:" }, { "vendor": "Maxtor", "size": 8388608, "volume": "C:" }], "monitor": null, "length": 42, "height": 21, "width": 54 };

  const url = req.url.slice(8); // remove /task3a at the beginning

  if (url.length == 0) {
    res.json(pc);
  } else {
    const parts = url.split('/');
    let currentObject = pc;
    let error = false;

    for (let i = 0; i < parts.length; i++) {
      if(parts[i] == "") {
        continue;
      }

      if (typeof currentObject == 'object' && parts[i] in currentObject) {
        if (Array.isArray(currentObject)) {
          const n = parseInt(parts[i]);
          if (isNaN(n)) {
            error = true;
          } else {
            currentObject = currentObject[n];         
          }
        } else {
          currentObject = currentObject[parts[i]];
        }
      } else {
        error = true;
      }
    }

    if (error) {
      res.status(404).send('Not Found');
    } else {
      res.json(currentObject);
    }
  }
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
